variable "devs" {
  description = "Please define list of nessosary instanse: <instanse_role>-<your_username>"
  type        = list(any)
  default = [
    "db-dvader",
    "app1-dvader"
  ]
}

######################### Possible instanse roles with sizes #########################
variable "plans" {
  description = "instance sizes"
  type        = map(any)
  default = {
    "lb"  = "s-1vcpu-1gb"
    "db"  = "s-1vcpu-2gb"
    "web" = "s-1vcpu-1gb"
    "app" = "s-2vcpu-2gb"
  }
}
