data "digitalocean_ssh_key" "USER_SSH_PUB_KEY" {
  name = "USER.SSH.PUB.KEY"
}
# API request
data "http" "ssh_search" {
  url = "https://api.digitalocean.com/v2/account/keys?per_page=200"
  # Optional request headers
  request_headers = {
    Content-Type  = "application/json"
    Authorization = "Bearer ${var.do_token}"

  }
}
# Search key ID through pub key 
# data "jq_query" "REBRAIN_SSH_PUB_KEY" {
#   data  = data.http.ssh_search.response_body
#   query = ".ssh_keys [] | select(.public_key == ${file("rebrain_key.pub")}) | .id"
# }

resource "digitalocean_tag" "task" {
  name = local.tags.task
}
resource "digitalocean_tag" "module" {
  name = local.tags.module
}
resource "digitalocean_tag" "user_email" {
  name = local.tags.user_email
}

# DO instance create
resource "random_password" "password" {
  count  = local.instance_count
  length = 16

}

resource "digitalocean_droplet" "instance" {
  count  = local.instance_count
  image  = "ubuntu-22-04-x64"
  name   = "${regex("[^-]*$", element(var.devs, count.index))}-${regex("[^-]*", element(var.devs, count.index))}"
  region = "ams3"
  size   = var.plans[regex("^[^(0-9)-]*", element(var.devs, count.index))]
  #  ssh_keys = [data.jq_query.REBRAIN_SSH_PUB_KEY.result, data.digitalocean_ssh_key.USER_SSH_PUB_KEY.id]
  ssh_keys = [data.digitalocean_ssh_key.USER_SSH_PUB_KEY.id]
  tags     = [resource.digitalocean_tag.task.id, resource.digitalocean_tag.module.id, resource.digitalocean_tag.user_email.id]

  connection {
    type        = "ssh"
    user        = "root"
    private_key = file(var.my_private_key)
    host        = self.ipv4_address
  }

  provisioner "remote-exec" {
    inline = [
      "sleep 5",
      "echo 'root:${resource.random_password.password[count.index].result}'|chpasswd",
      "sed -i 's/^PasswordAuthentication .*$/PasswordAuthentication yes/' '/etc/ssh/sshd_config'",
      "rm /etc/ssh/sshd_config.d/*",
      "systemctl restart sshd",
    ]
  }
}

resource "local_file" "ansible_hosts_yaml" {
  content = templatefile("${path.module}/ansible_hosts_template.tftpl",
    {
      server_name                  = resource.digitalocean_droplet.instance[*].name
      ansible_host                 = resource.aws_route53_record.rebrain_user[*].fqdn
      ansible_user                 = "root"
      ansible_ssh_private_key_file = var.my_private_key
    }
  )
  filename = "ansible/hosts.yaml"
}
