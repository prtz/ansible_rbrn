locals {
  tags = {
    task       = "ansible-01"
    module     = "devops"
    user_email = var.gitlab_username
  }
}
