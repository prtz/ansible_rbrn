# Get zone info
data "aws_route53_zone" "devops_rebrain_srwx_net" {
  name = "devops.rebrain.srwx.net."
}
# Add DNS record
resource "aws_route53_record" "rebrain_user" {
  count   = local.instance_count
  zone_id = data.aws_route53_zone.devops_rebrain_srwx_net.zone_id
  name    = "${regex("[^-]*$", element(var.devs, count.index))}-${regex("[^-]*", element(var.devs, count.index))}."
  type    = "A"
  ttl     = 300
  records = [element(local.do_instance_ip_addr, count.index)]
}
