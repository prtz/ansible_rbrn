locals {
  do_instance_ip_addr  = resource.digitalocean_droplet.instance[*].ipv4_address
  do_instance_password = resource.random_password.password[*].result
  do_instance_fqdn     = resource.aws_route53_record.rebrain_user[*].fqdn
  instance_count       = length(var.devs)
}

