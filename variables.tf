#######################################################
#### Variables to configure (or be prompted about) ####
#######################################################

####################### GitLab ########################
variable "gitlab_token" {
  description = "your gitlab token"
  sensitive   = true
}

variable "gitlab_username" {
  description = "your rebrain gitlab username"
  sensitive   = true
}

variable "key" {
  description = "your ssh public key that will add to GitLab project"
  sensitive   = true
}

variable "rebrain_devops_terraform_task4_id" {
  description = "nessesary repository ID"
}

####################### DigtalOcean ###################
variable "do_token" {
  description = "your DO token"
  sensitive   = true
}

variable "my_pub_key" {
  description = "your pub SSH key for add to DO"
  sensitive   = true
}

variable "my_private_key" {
  description = "file name of your private SSH key for access in to instance"
}

variable "rebraine_ssh_pub_key" {
  description = "your pub SSH key for add to DO"
  sensitive   = true
  type        = string
}

# variable "instance_count" {
#   description = "nessesary instance count"
#   default     = length(var.devs)
# }

######################### AWS #########################
variable "aws-access-key" {
  description = "your AWS access key"
  sensitive   = true
}

variable "aws-secret-key" {
  description = "your AWS secret key"
  sensitive   = true
}
#######################################################
######### Variables you may want to configure #########
#######################################################

variable "gitlab_base_url" {
  description = "gitlab API endpoint"
  default     = "https://gitlab.rebrainme.com/api/v4/"
}



