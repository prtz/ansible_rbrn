terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.18.0"
    }
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.22.3"
    }
    http = {
      source  = "hashicorp/http"
      version = "3.1.0"
    }
    jq = {
      source  = "massdriver-cloud/jq"
      version = "0.2.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "4.31.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.4.3"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.2.3"
    }
    template = {
      source  = "hashicorp/template"
      version = "2.2.0"
    }
  }
}
provider "gitlab" {
  token    = var.gitlab_token
  base_url = var.gitlab_base_url
}
provider "digitalocean" {
  token = var.do_token
}

provider "aws" {
  region     = "ap-southeast-1"
  access_key = var.aws-access-key
  secret_key = var.aws-secret-key
}


